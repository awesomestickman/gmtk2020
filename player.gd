extends KinematicBody

const MOVE_SPEED = 4
const MOUSE_SENS = 0.5

onready var anim_player = $AnimationPlayer
onready var raycast = $RayCast

func _ready():
	#mouse invisible and stuck in middle of screen
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	#pauses the game state - in this case waits one frame
	yield(get_tree(), "idle_frame")
	#call setplayer on all the enemies
	get_tree().call_group("zombies", "set_player", self)

#func _input(event):
	#if event is InputEventMouseMotion:
		#get how much we moved mouse on x axis then multiply that by mouse_sensitiviy
		#then apply the rotation, using degrees instead of radians
		#rotation_degrees.y -= MOUSE_SENS * event.relative.x
		
func _process(delta):
	if Input.is_action_pressed("exit"):
		get_tree().quit()
	if Input.is_action_pressed("restart"):
		kill()
	
	#Compass Icon that points towards $Goal in World.tscn
	$Compass.look_at(get_parent().get_node("Goal").get_translation(), Vector3(0,4,0))

func _physics_process(delta):
	var move_vec = Vector3()
	
	if Input.is_action_pressed("move_forwards"):
		PlayerVariables._accel(0.5)
	if Input.is_action_pressed("move_backwards"):
		PlayerVariables._decel()
		
	if Input.is_action_pressed("turn_left"):
		#rotation_degrees.y += deg2rad(60)
		PlayerVariables._set_wheel_turn_curr(PlayerVariables._get_wheel_turn_left_max())
	if Input.is_action_pressed("turn_right"):
		PlayerVariables._set_wheel_turn_curr(PlayerVariables._get_wheel_turn_right_max())
		#rotation_degrees.y -= deg2rad(60)
		
	if Input.is_action_just_released("turn_left"):
		PlayerVariables._set_wheel_turn_curr(0)
	if Input.is_action_just_released("turn_right"):
		PlayerVariables._set_wheel_turn_curr(0)
		
	if PlayerVariables._get_vel() != 0:
		rotation_degrees.y += PlayerVariables._get_wheel_turn_curr() * PlayerVariables._get_wheel_turn_sensitivity() * abs(PlayerVariables._get_vel() * PlayerVariables._get_vel_multiplier() )
		
	move_vec.z -= PlayerVariables._get_vel()
	
	#normalize so don't move faster diagnolly
	move_vec = move_vec.normalized()
	#rotate along y axis at our current rotation
	move_vec = move_vec.rotated(Vector3(0,1,0),rotation.y)
	#then move
	move_and_collide(move_vec * MOVE_SPEED * delta)
	
	if Input.is_action_pressed("shoot") and !anim_player.is_playing():
		anim_player.play("shoot")
		var coll = raycast.get_collider()
		if raycast.is_colliding() and coll.has_method("kill"):
			coll.kill()

func kill():
	get_tree().reload_current_scene()
