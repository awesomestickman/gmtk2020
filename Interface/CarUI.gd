#CarUI.gd
extends Control

func _ready():
	#World Reset
	Globals._set_damage(0)
	$Car_Roof/Roof_Anim.stop()
	$Car_Bobblehead/Bobble_Anim.stop()
	$Car_Brake/Brake_Anim.stop()
	$Car_Handle/Handle_Anim.stop()
	$Car_Pedals/Pedals_Anim.stop()
	$Car_Wheel/Wheel_Anim.stop()
	$CarAudio.play()

func _process(delta):
	#Debug Display
	$Debug/VBox/Damage.text = "Damage: " + str(Globals._get_damage())

func _input(event):
	#Controls
	#Rotate Wheel in Direction Pressed
	if event.is_action_pressed("turn_left"):
		$Car_Wheel.set_rotation(deg2rad(-60))
	if event.is_action_pressed("turn_right"):
		$Car_Wheel.set_rotation(deg2rad(60))
	
	#Reset Wheel Rotation
	if event.is_action_released("turn_left"):
		$Car_Wheel.set_rotation(0)
	if event.is_action_released("turn_right"):
		$Car_Wheel.set_rotation(0)
	
	#Debug Controls
	if event.is_action_pressed("damage"):
		Globals._set_damage(Globals._get_damage() + 1)
		_pop_UI()

#Car Deterioration
func _pop_UI():
	if (Globals._get_damage() == 1):
		$Car_Roof/Roof_Anim.play("Car_Roof_Anim")
	if (Globals._get_damage() == 2):
		$Car_Bobblehead/Bobble_Anim.play("Bobble_Anim")
	if (Globals._get_damage() == 3):
		$Car_Pedals/Pedals_Anim.play("Pedals_Anim")
	if (Globals._get_damage() == 4):
		$Car_Handle/Handle_Anim.play("Handle_Anim")
	if (Globals._get_damage() == 5):
		$Car_Brake/Brake_Anim.play("Brake_Anim")
	if (Globals._get_damage() == 6):
		$Car_Wheel/Wheel_Anim.play("Wheel_Anim")
