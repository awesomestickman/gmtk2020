extends Node


# Variables

var curr_player_velocity = 0
var decel_rate = 0.2

var wheel_turn_right_max = -60
var wheel_turn_left_max = 60
var wheel_turn_current = 0
var wheel_turn_sensitivity = 0.02
var wheel_turn_vel_multiplier = 0.05
var MAX_SPEED = 30

func _get_vel():
	return curr_player_velocity

func _get_vel_multiplier():
	return wheel_turn_vel_multiplier

func _accel(amt):
	if curr_player_velocity <= MAX_SPEED:
		curr_player_velocity += amt
	

func _decel():
	curr_player_velocity -= curr_player_velocity * decel_rate
	if curr_player_velocity < 1:
		curr_player_velocity = 0 

func _get_wheel_turn_left_max():
	return wheel_turn_left_max

func _get_wheel_turn_right_max():
	return wheel_turn_right_max

func _get_wheel_turn_sensitivity():
	return wheel_turn_sensitivity

func _get_wheel_turn_curr():
	return wheel_turn_current
	
func _set_wheel_turn_curr(amt):
	wheel_turn_current = amt
